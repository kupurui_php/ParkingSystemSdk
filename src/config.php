<?php
//配置硬件厂商对接SDK文件
return [
	//海康威视
	'hikvision'	=>	[
		[
			'name'	=>	'海康威视',
			'version'=>'v1_0',
			'sdk'	=>	'hikvision_v1_0'
		],
	],
	//立方
	'reformer'	=>	[
	    [
	        'name'=>'立方停车场',
            'version'=>'v2_0_9',
            'sdk'	=>	'reformer_v2_0_9'
        ]
    ],
	//科拓
	'keytop'	=>	[],
	//银联
	'unionpay'	=>	[],
	//富士
	'fujica'	=>	[],
];