<?php
namespace Kpr;

/**
 * 返回状态值及描述
 * Class CodeList
 * @package Kpr
 */
class CodeList{
	//所有状态值列表
	protected static $codeList=[
		0	=>	'正常',
        -1	=>	'返回数据错误或未返回数据',
		1	=>	'请求硬件商失败',
		2	=>	'硬件商处理失败',
        3   =>  '未获取到请求参数',
        4   =>  '接口不存在',
	];

	//正常
	public static $SUCCESS = 0;

	//请求硬件商失败,网络错误或服务商服务器内部错误
	public static $NET_ERROR = 1;

	//硬件商处理失败,即发生了正常的请求并正常的响应了，但是响应状态是失败
	public static $HAND_ERROR = 2;

	//调用接口时没有获取到接口所需的必传参数
    public static  $PARAMETER_ERROR = 3;

    //调用接口时没有获取到接口所需的必传参数
    public static  $API_ERROR = -1;

    //当前停车场不支持该功能
    public static $NOT_API = 4;

	/**
	 * 获取CODE描述
	 * @param $code
	 * @return mixed|string
	 */
	public static function getCodeInfo($code) {
		if (array_key_exists($code, self::$codeList)) {
			return self::$codeList[$code];
		}
		return '';
	}
}