<?php
namespace Kpr;

/**
 * 工具类
 * Class KprTool
 * @package Kpr
 */
class KprTool{

	/**
	 * CURL POST请求
	 * @param $url
	 * @param $data
	 * @param $header
	 * @param bool $isSsl      //是否需要证书
	 * @return bool|string
	 * @throws \Exception
	 */
	public static function curlPost($url, $data, $header=[], $isSsl = false, $certs=['apiclient_cert'=>'','apiclient_key'=>'','cacert'=>'']) {
		$ch = curl_init();
		$timeout = 600;
		if(!empty($header)){
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		}
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json; charset=utf-8',
            )
        );
		if ($isSsl) {
			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,1);//证书检查
			curl_setopt($ch,CURLOPT_SSLCERTTYPE,'pem');
//			curl_setopt($ch,CURLOPT_SSLCERT,dirname(__FILE__).'/../cert/apiclient_cert.pem');
			curl_setopt($ch,CURLOPT_SSLCERT,$certs['apiclient_cert']);
			curl_setopt($ch,CURLOPT_SSLCERTTYPE,'pem');
//			curl_setopt($ch,CURLOPT_SSLKEY,dirname(__FILE__).'/../cert/apiclient_key.pem');
			curl_setopt($ch,CURLOPT_SSLCERT,$certs['apiclient_key']);
			curl_setopt($ch,CURLOPT_SSLCERTTYPE,'pem');
//			curl_setopt($ch,CURLOPT_CAINFO,dirname(__FILE__).'/../cert/cacert.pem');
			curl_setopt($ch,CURLOPT_SSLCERT,$certs['cacert']);
		} else {
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		}

		$reponse = curl_exec($ch);
		if (curl_errno($ch)) {
			throw new \Exception(curl_error($ch), 0);
		} else {
			$httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if (200 !== $httpStatusCode) {
				throw new \Exception($reponse, $httpStatusCode);
			}
		}
		curl_close($ch);
		return $reponse;
	}

	/**
	 * CURL GET请求
	 * @param $url
	 * @return bool|string
	 * @throws \Exception
	 */
	public static function curlGet($url, $header=[]){
		$curl = curl_init();
		if(!empty($header)){
			curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		}
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

		$reponse = curl_exec($curl);
		if (curl_errno($curl)) {
			throw new \Exception(curl_error($curl), 0);
		} else {
			$httpStatusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			if (200 !== $httpStatusCode) {
				throw new \Exception($reponse, $httpStatusCode);
			}
		}
		curl_close($curl);
		return $reponse;
	}

	/**
	 * 将XML转换为ARRAY
	 * @param $xml
	 * @return mixed
	 */
	public static function xmlToArr($xml)
	{
		$xmlObj = simplexml_load_string($xml,'SimpleXMLElement', LIBXML_NOCDATA);
		return json_decode(json_encode($xmlObj,JSON_UNESCAPED_UNICODE), true);
	}

	/**
	 * 将传ARRAY转换成XML
	 * @param $arr
	 * @return string
	 */
	public static function arrayToXml($arr) {
		$rel = '<xml>';
		foreach ($arr as $key => $value) {
			$rel .= '<' . $key . '>' . $value . '</' . $key . '>';
		}
		$rel .= '</xml>';
		return $rel;
	}


    /**
     * 输出结果
     * @param int $code 状态码
     * @param string $msg 提示消息
     * @return array $data  返回数据
     */
    public static function outPut($code=0, $msg='', $data=[], $is_die=true){
        $ret = array(
            'code'=>$code,
            'message'=>$msg,
            'data'=>$data
        );
        if($is_die){
            die(json_encode($ret));
        }
        return $ret;
    }
}