<?php
namespace Kpr;

use Kpr\interfaces\BaseInterface;

/**
 * 工厂类
 * Class Factory
 * @package kpr
 */
class Factory{
	//对象实例列表
	private static $objList = [];

	//类映射表
	protected static $mapList = [];

	private function __construct(){}

	/**
	 * 获取SDK对象
	 * @param $sdkName
	 * @param $sdkVersion
	 * @param array $params
	 * @return mixed
	 */
	public static function getSdkObj($sdkName, $sdkVersion, $params=[]) {
		if (!is_string($sdkName) || !is_string($sdkVersion)) {
			throw new \Error('getSdkObj function params must is string!');
		}

		$tempName = md5($sdkName . '_' . $sdkVersion);
		if (!array_key_exists($tempName, self::$objList)) {
			$namespaceName = self::getMapList($sdkName, $sdkVersion);
			if ($namespaceName === false) {
				throw new \Error("{$sdkName} {$sdkVersion} version class not find!");
			}
			if ($params) {
				isset($params['appKey']) OR $params['appKey']='';
				isset($params['appScript']) OR $params['appScript']='';
				isset($params['isAddPwd']) OR $params['isAddPwd']=false;
				isset($params['apiUrl']) OR $params['apiUrl']='';
			}

			try{
				$obj  = new $namespaceName($params['appKey'], $params['appScript'], $params['isAddPwd'], $params['apiUrl']);
				if ($obj instanceof BaseInterface) {
					self::$objList[$tempName] = $obj;
				} else {
					throw new \Error($namespaceName .' not instanceof Kpr\\instanceof\\BaseInterface');
				}

				if (isset($params['otherParams']) && is_array($params['otherParams'])) {
					foreach ($params['otherParams'] as $key => $value) {
						self::$objList[$tempName]->addOtherParams($key, $value);
					}
				}
			} catch (\Exception $e) {
				throw new \Error($e->getMessage());
			}

		}
		return self::$objList[$tempName];
	}

	/**
	 * 获取SDK映射文件地址
	 * @param $sdkName
	 * @param $sdkVersion
	 * @return bool|mixed
	 */
	protected static function getMapList($sdkName, $sdkVersion) {
		self::getConfig();


		if (!array_key_exists($sdkName, self::$mapList)) {
			return false;
		}

		foreach (self::$mapList[$sdkName] as $item) {
			if ($item['version']===$sdkVersion) {
				return isset($item['sdk']) ? 'Kpr\\lib\\'.$item['sdk'] : false;
			}
		}

		return false;
	}

	/**
	 * 获取配置
	 * @return array|mixed
	 */
	protected static function getConfig() {
		if (!self::$mapList) {
			self::$mapList = include(__DIR__ . '/config.php');
		}
		return self::$mapList;
	}

	/**
	 * 获取支持的所有硬件厂家SDK及版本号
	 * @return array
	 */
	public static function getAllFactoryList() {
		$config = self::getConfig();
		$relData = [];
		foreach ($config as $key => $item) {
			foreach ($item as $k1 => $v1) {
				$relData[$key][] = [
					'name'	=>	$v1['name'],
					'version'	=>	$v1['version']
				];
			}
		}

		return $relData;
	}
}