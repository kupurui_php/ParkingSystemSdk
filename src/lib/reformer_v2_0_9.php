<?php


namespace Kpr\lib;


use Kpr\CodeList;
use Kpr\interfaces\BaseInterface;
use Kpr\KprTool;

class reformer_v2_0_9 extends BaseInterface
{

    //* 此属性只能是config文件中的key
    public $nowFactory = 'reformer';

    //* 其他参数存放
    public $otherParams = [];

    //* 当前硬件API库版本号
    public $version='v2_0_9';

    //* 当前硬件库的所有请求路由列表
    public $allRouteList = [
        'getParkingInfos'=>'GetParkingLotInfo',
        'getCarTimeInfo'=>'GetCarInfo',
        'getChargeBill'=>'GetCarInfo',
        'payVehilceBill'=>'AddChargeInfo',
        'openGate'=>'GetGateControlInfo',
        'closeGate'=>'GetGateControlInfo',
        'carRecharge'=>'RenewalLongUser',
    ];

    //* 当前硬件库描述（某个厂家的某个模块服务，版本号，其他开发人员调用需要注意的点）
    public $describe = "这里是海康SDKv1.0";

    /**
     * 构造函数 使用参数设置函数进行设置 注意此函数无返回值
     * BaseInterface constructor.
     * @param string $appKey
     * @param string $appScript
     * @param bool $isAddPwd
     * @param string $apiUrl
     */
    public function __construct($appKey = '', $appScript = '', $isAddPwd = false, $apiUrl = '')
    {
        $this->setAppKey($appKey);
        $this->setAppScript($appScript);
        $this->setIsAddPwd($isAddPwd);
        $this->setApiUrl($apiUrl);
    }

    /**
     * 获取地址信息
     * @param string $uri
     * @param string $key
     * @return string
     */
    private function getUrl($uri = '',$key='')
    {
        $url = '';
        if (empty($uri)){
            if (empty($key)){
                return KprTool::outPut(CodeList::$PARAMETER_ERROR,CodeList::getCodeInfo(CodeList::$PARAMETER_ERROR));
            }
            $url = rtrim($this->apiUrl, '/').'/'. ltrim($this->getAllRouteList($key),'/');
        }else{
            $url = $uri;
        }
        return $url;
    }


    /**
     * 查询停车场车位信息 剩余车位、总车位
     * @param $params
     * @param $uri 当前方法对外访问的URI
     * @return mixed [['total'=>'int 总车位','surplus'=>'int 剩余车位','regionName'=>'string 楼层名、区域名、地址名等','regionId'=>'string 如果存在楼层ID，区域ID，地址ID等填写，不存在则默认值-1'],]
     */
    public function getParkingInfos($params, $uri = '')
    {
        // TODO: Implement getParkingInfos() method.

        $api_url = $this->getUrl($uri,'getParkingInfos');
        $res = KprTool::curlPost($api_url,json_encode($params));
        $ret = json_decode($res,true);
        if(!$ret || $ret['resCode'] != '0'){
            KprTool::outPut(CodeList::$API_ERROR,$ret['resMsg']);
        }
        $data = [];
        foreach ($ret['parkingLotInfo'] as $k=>$v){
            $data[$k]['total'] = $v['totalNum'];
            $data[$k]['surplus'] = $v['totalRemainNum'];
            $data[$k]['regionName'] = $v['parkingLotName'];
            $data[$k]['regionId'] = $v['parkingLotId'];
        }
        return KprTool::outPut(CodeList::$SUCCESS,CodeList::getCodeInfo(CodeList::$SUCCESS),$data);
    }

    /**
     * 查询车辆信息 车辆进场时间、停车时长
     * @param $params
     * @param $uri 当前方法对外访问的URI
     * @return mixed ['parkName'=>'string 停车场名称','plateNo'=>'string 车牌号','enterTime'=>'int 入场时间(时间戳)','parkingTime'=>'int 停车时长(秒)']
     */
    public function getCarTimeInfo($params, $uri = '')
    {
        // TODO: Implement getCarTimeInfo() method.
        $api_url = $this->getUrl($uri,'getParkingInfos');
        $res = KprTool::curlPost($api_url,json_encode($params));
        $ret = json_decode($res,true);
        if(!$ret || $ret['resCode'] != '0'){
            KprTool::outPut(CodeList::$API_ERROR,$ret['resMsg']);
        }
        $data = [
            'parkName'=>'停车场名称',
            'plateNo' => $ret['carCode'],
            'enterTime' => date('Y-m-d H:i:s',strtotime($res['inTime'])),
            'parkingTime' => $this->dateDistance(date('Y-m-d H:i:s',strtotime($res['inTime'])),date('Y-m-d H:i:s')),
        ];
        return KprTool::outPut(CodeList::$SUCCESS,CodeList::getCodeInfo(CodeList::$SUCCESS),$data);
    }

    /**
     * 查询车辆停车费用
     * @param $params
     * @param $uri 当前方法对外访问的URI
     * @return mixed ['plateNo'=>'string 车牌号','parkName'=>'string 停车场名称', 'enterTime'=>'int 入场时间（时间戳）',
     * 'parkingTime'=>'int 停车时长（秒）','cost'=>'float 应付金额','totalCost'=>'float 总收费金额',
     * 'paidCost'=>'float 已支付金额','delayTime'=>'int 缴费后允许延时出场时间(秒)']
     */
    public function getChargeBill($params, $uri = '')
    {
        // TODO: Implement getChargeBill() method.
        $api_url = $this->getUrl($uri,'getChargeBill');
        $res = KprTool::curlPost($api_url,json_encode($params));
        $ret = json_decode($res,true);
        if(!$ret || $ret['resCode'] != '0'){
            KprTool::outPut(CodeList::$API_ERROR,$ret['resMsg']);
        }
        $data = [
            'parkName'=>$ret[''],
            'plateNo' => $ret['carCode'],
            'enterTime' => date('Y-m-d H:i:s',strtotime($res['inTime'])),
            'parkingTime' => $this->dateDistance(date('Y-m-d H:i:s',strtotime($res['inTime'])),date('Y-m-d H:i:s')),
            'cost'=>$ret['paidMoney'],
            'totalCost'=>$ret['chargeMoney'],
            'paidCost'=>0,
            'delayTime'=>$ret['remainLeaveTime']
        ];
        return KprTool::outPut(CodeList::$SUCCESS,CodeList::getCodeInfo(CodeList::$SUCCESS),$data);
    }

    /**
     * 费用上报 缴费成功后同步到硬件商服务器
     * @param $params
     * @param $uri 当前方法对外访问的URI
     * @return mixed ['totalCost'=>'float 总收费金额','realCost'=>'float 应收金额', 'cost'=>'float 实收金额','state'=>'int 支付状态 1:已支付 0:未支付', 'costTime'=>'int 缴费时间（时间戳）', 'parkName'=>'string 停车场名称']
     */
    public function payVehilceBill($params, $uri = '')
    {
        // TODO: Implement payVehilceBill() method.
        $api_url = $this->getUrl($uri,'payVehilceBill');
        $res = KprTool::curlPost($api_url,json_encode($params));
        $ret = json_decode($res,true);
        if(!$ret || $ret['resCode'] != '0'){
            KprTool::outPut(CodeList::$API_ERROR,$ret['resMsg']);
        }
        $data = [
            'totalCost'=>$params['amount'],
            'realCost'=>$params['amount'],
            'cost'=>$params['amount'],
            'state'=>'1',
            'costTime'=>$params['get_time'],
            'parkName'=>''
        ];
        return KprTool::outPut(CodeList::$SUCCESS,CodeList::getCodeInfo(CodeList::$SUCCESS),$data);
    }

    /**
     * 手动开闸
     * @param $params
     * @param $uri 当前方法对外访问的URI
     * @return mixed
     */
    public function openGate($params, $uri = '')
    {
        // TODO: Implement openGate() method.
        $api_url = $this->getUrl($uri,'openGate');
        $res = KprTool::curlPost($api_url,json_encode($params));
        $ret = json_decode($res,true);
        if(!$ret || $ret['resCode'] != '0'){
            KprTool::outPut(CodeList::$API_ERROR,$ret['resMsg']);
        }
        return KprTool::outPut(CodeList::$SUCCESS,CodeList::getCodeInfo(CodeList::$SUCCESS));
    }

    /**
     * 手动关闸
     * @param $params
     * @param $uri 当前方法对外访问的URI
     * @return mixed
     */
    public function closeGate($params, $uri = '')
    {
        // TODO: Implement closeGate() method.
        $api_url = $this->getUrl($uri,'closeGate');
        $res = KprTool::curlPost($api_url,json_encode($params));
        $ret = json_decode($res,true);
        if(!$ret || $ret['resCode'] != '0'){
            KprTool::outPut(CodeList::$API_ERROR,$ret['resMsg']);
        }
        return KprTool::outPut(CodeList::$SUCCESS,CodeList::getCodeInfo(CodeList::$SUCCESS));
    }

    /**
     * 固定车位续费
     * @param $params
     * @param string $uri
     * @return mixed
     */
    public function carRecharge($params, $uri = '')
    {
        // TODO: Implement carRecharge() method.
//        $params = [
//            'carCode'=>'渝AR2N97',           //车牌号码
//            'chargeMoney'=>'0.01',           //续费金额（单位：分）
//            'endTime'=>'2020-09-24 23:59:59',           //续费有效结束时间（格式：yyyy-MM-dd 23:59:59）
//            'payTime'=>'2020-08-24 13:41:52',           //收费时间（格式：yyyy-MM-dd HH:mm:ss）
//            'chargeType'=>11,           //收费方式(3:手持机，11:第三方接口收费)
//            'amountType'=>1,           //费用类型(默认：1)
//            'chargeSource'=>1,           //详见”附录2收费来源（chargeSource）字典”
//        ];
        $api_url = $this->getUrl($uri,'carRecharge');
        $res = KprTool::curlPost($api_url,json_encode($params));
        $ret = json_decode($res,true);
        if(!$ret || $ret['resCode'] != '0'){
            KprTool::outPut(CodeList::$API_ERROR,$ret['resMsg']);
        }
        return KprTool::outPut(CodeList::$SUCCESS,CodeList::getCodeInfo(CodeList::$SUCCESS));
    }

    /**
     * 固定车位添加
     * @param $params
     * @param string $uri
     * @return mixed
     */
    public function addCarInfo($params, $uri = '')
    {
        // TODO: Implement addCarInfo() method.
        return KprTool::outPut(CodeList::$NOT_API,CodeList::getCodeInfo(CodeList::$NOT_API));
    }

    /**
     * 固定车位取消
     * @param $params
     * @param string $uri
     * @return mixed
     */
    public function deleteCarInfo($params, $uri = '')
    {
        // TODO: Implement deleteCarInfo() method.
        return KprTool::outPut(CodeList::$NOT_API,CodeList::getCodeInfo(CodeList::$NOT_API));
    }

    /**
     * 预约车位
     * @param $params
     * @param string $uri
     * @return mixed ['parkName'=>'string 停车场名称','plateNo'=>'string 车牌号码', 'status'=>'int 0:已预约 1:用户取消 2:到期 3:已进场', 'startTime'=>'int 预约开始时间(时间戳)', 'endTime'=>'int 结束时间(时间戳)','phoneNo'=>'string 联系电话']
     */
    public function reserveParkingPlot($params, $uri = '')
    {
        // TODO: Implement reserveParkingPlot() method.
        return KprTool::outPut(CodeList::$NOT_API,CodeList::getCodeInfo(CodeList::$NOT_API));
    }

    /**
     * 取消预约车位
     * @param $params
     * @param string $uri
     * @return mixed
     */
    public function cancelReserve($params, $uri = '')
    {
        // TODO: Implement cancelReserve() method.
        return KprTool::outPut(CodeList::$NOT_API,CodeList::getCodeInfo(CodeList::$NOT_API));
    }


    /**
     * 停留时长计算
     */
    public function dateDistance($first,$second) {
        $_start = $first;
        $_end = $second;
        $_start_time = strtotime($_start);
        $_end_time = strtotime($_end);
        $_timestamp = $_end_time-$_start_time;
        $_days = floor($_timestamp / 86400);
        $_hous = floor($_timestamp % 86400 / 3600);
        $_mintues = floor($_timestamp % 86400 % 3600 / 60);

        $str = '';
        if($_days > 0){
            $str .= $_days.'天';
        }
        if($_hous > 0){
            $str .= $_hous.'小时';
        }
        $str .= $_mintues.'分钟';
        return $str;
    }

}