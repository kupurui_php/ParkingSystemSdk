<?php
namespace Kpr\lib;

use Kpr\interfaces\BaseInterface;

class hikvision_v1_0 extends BaseInterface{

	//* 此属性只能是config文件中的key
	public $nowFactory = 'hikvision';

	//* 其他参数存放
	public $otherParams = [];

	//* 当前硬件API库版本号
	public $version='v1_0';

	//* 当前硬件库的所有请求路由列表
	public $allRouteList = [];

	//* 当前硬件库描述（某个厂家的某个模块服务，版本号，其他开发人员调用需要注意的点）
	public $describe = "这里是海康SDKv1.0";

	/**
	 * 构造函数 使用参数设置函数进行设置 注意此函数无返回值
	 * BaseInterface constructor.
	 * @param string $appKey
	 * @param string $appScript
	 * @param bool $isAddPwd
	 * @param string $apiUrl
	 */
	public function __construct($appKey = '', $appScript = '', $isAddPwd = false, $apiUrl = '')
	{
		$this->setAppKey($appKey);
		$this->setAppScript($appScript);
		$this->setIsAddPwd($isAddPwd);
		$this->setApiUrl($apiUrl);
	}

	/**
	 * 查询停车场车位信息 剩余车位、总车位
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed [['total'=>'int 总车位','surplus'=>'int 剩余车位','regionName'=>'string 楼层名、区域名、地址名等','regionId'=>'string 如果存在楼层ID，区域ID，地址ID等填写，不存在则默认值-1'],]
	 */
	public function getParkingInfos($params, $uri = '')
	{
		// TODO: Implement getParkingInfos() method.
	}

	/**
	 * 查询车辆信息 车辆进场时间、停车时长
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed ['parkName'=>'string 停车场名称','plateNo'=>'string 车牌号','enterTime'=>'int 入场时间(时间戳)','parkingTime'=>'int 停车时长(秒)']
	 */
	public function getCarTimeInfo($params, $uri = '')
	{
		// TODO: Implement getCarTimeInfo() method.
	}

	/**
	 * 查询车辆停车费用
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed ['plateNo'=>'string 车牌号','parkName'=>'string 停车场名称', 'enterTime'=>'int 入场时间（时间戳）','parkingTime'=>'int 停车时长（秒）','cost'=>'float 应付金额','totalCost'=>'float 总收费金额', 'paidCost'=>'float 已支付金额','delayTime'=>'int 缴费后允许延时出场时间(秒)']
	 */
	public function getChargeBill($params, $uri = '')
	{
		// TODO: Implement getChargeBill() method.
	}

	/**
	 * 费用上报 缴费成功后同步到硬件商服务器
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed ['totalCost'=>'float 总收费金额','realCost'=>'float 应收金额', 'cost'=>'float 实收金额','state'=>'int 支付状态 1:已支付 0:未支付', 'costTime'=>'int 缴费时间（时间戳）', 'parkName'=>'string 停车场名称']
	 */
	public function payVehilceBill($params, $uri = '')
	{
		// TODO: Implement payVehilceBill() method.
	}

	/**
	 * 手动开闸
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed
	 */
	public function openGate($params, $uri = '')
	{
		// TODO: Implement openGate() method.
	}

	/**
	 * 手动关闸
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed
	 */
	public function closeGate($params, $uri = '')
	{
		// TODO: Implement closeGate() method.
	}

	/**
	 * 固定车位续费
	 * @param $params
	 * @param string $uri
	 * @return mixed
	 */
	public function carRecharge($params, $uri = '')
	{
		// TODO: Implement carRecharge() method.
	}

	/**
	 * 固定车位添加
	 * @param $params
	 * @param string $uri
	 * @return mixed
	 */
	public function addCarInfo($params, $uri = '')
	{
		// TODO: Implement addCarInfo() method.
	}

	/**
	 * 固定车位取消
	 * @param $params
	 * @param string $uri
	 * @return mixed
	 */
	public function deleteCarInfo($params, $uri = '')
	{
		// TODO: Implement deleteCarInfo() method.
	}

	/**
	 * 预约车位
	 * @param $params
	 * @param string $uri
	 * @return mixed ['parkName'=>'string 停车场名称','plateNo'=>'string 车牌号码', 'status'=>'int 0:已预约 1:用户取消 2:到期 3:已进场', 'startTime'=>'int 预约开始时间(时间戳)', 'endTime'=>'int 结束时间(时间戳)','phoneNo'=>'string 联系电话']
	 */
	public function reserveParkingPlot($params, $uri = '')
	{
		// TODO: Implement reserveParkingPlot() method.
	}

	/**
	 * 取消预约车位
	 * @param $params
	 * @param string $uri
	 * @return mixed
	 */
	public function cancelReserve($params, $uri = '')
	{
		// TODO: Implement cancelReserve() method.
	}

}