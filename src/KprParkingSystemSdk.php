<?php
namespace Kpr;

/**
 * SDK对外统一接口控制器
 * Class KprParkingSystemSdk
 * @package Kpr
 */
class KprParkingSystemSdk {
	//当前SDK对象
	protected $sdkObj = '';

	/**
	 * 当前扩展版本号
	 * @return string
	 */
	public static function version() {
		return '1.0.0';
	}

	public function __construct($sdkName, $sdkVersion, $appKey='', $appScript='', $isAddPwd=false, $apiUrl='', $otherParams=[])
	{
        $isAddPwd = (bool)$isAddPwd;
		$params = compact('appKey', 'appScript', 'isAddPwd', 'apiUrl', 'otherParams');
		$this->sdkObj = Factory::getSdkObj($sdkName, $sdkVersion, $params);
	}

	/**
	 * 获取当前SDK对象 当部分SDK需要扩展公共类中没有的方法时就可以通过此对象去直接调用对应的方法
	 * @return mixed|string
	 */
	public function getSdkObj() {
		return $this->sdkObj;
	}

	public function __call($name, $arguments)
	{
		//因为本身检查如果不存在就需要抛出异常，所以就直接不检查直接转发到对象上调用，将错误交给系统处理
		$this->sdkObj->{$name}(...$arguments);
	}

	/**
	 * 获取所有的支持的硬件厂家SDK及版本
	 * @return array
	 */
	public static function getAllSdkAndVersion() {
		return [
			'code'	=>	0,
			'msg'	=>	'所有的硬件厂家SDK及版本',
			'data'	=>	Factory::getAllFactoryList()
		];
	}

	/**
	 * 查询停车场车位信息 剩余车位、总车位
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed [['total'=>'int 总车位','surplus'=>'int 剩余车位','regionName'=>'string 楼层名、区域名、地址名等','regionId'=>'string 如果存在楼层ID，区域ID，地址ID等填写，不存在则默认值-1'],]
	 */
	public function getParkingInfos($params, $uri = '')
	{
		// TODO: Implement getParkingInfos() method.
		return $this->sdkObj->getParkingInfos($params, $uri);
	}

	/**
	 * 查询车辆信息 车辆进场时间、停车时长
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed ['parkName'=>'string 停车场名称','plateNo'=>'string 车牌号','enterTime'=>'int 入场时间(时间戳)','parkingTime'=>'int 停车时长(秒)']
	 */
	public function getCarTimeInfo($params, $uri = '')
	{
		// TODO: Implement getCarTimeInfo() method.
		return $this->sdkObj->getCarTimeInfo($params, $uri);
	}

	/**
	 * 查询车辆停车费用
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed ['plateNo'=>'string 车牌号','parkName'=>'string 停车场名称', 'enterTime'=>'int 入场时间（时间戳）','parkingTime'=>'int 停车时长（秒）','cost'=>'float 应付金额','totalCost'=>'float 总收费金额', 'paidCost'=>'float 已支付金额','delayTime'=>'int 缴费后允许延时出场时间(秒)']
	 */
	public function getChargeBill($params, $uri = '')
	{
		// TODO: Implement getChargeBill() method.
		return $this->sdkObj->getChargeBill($params, $uri);
	}

	/**
	 * 费用上报 缴费成功后同步到硬件商服务器
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed ['totalCost'=>'float 总收费金额','realCost'=>'float 应收金额', 'cost'=>'float 实收金额','state'=>'int 支付状态 1:已支付 0:未支付', 'costTime'=>'int 缴费时间（时间戳）', 'parkName'=>'string 停车场名称']
	 */
	public function payVehilceBill($params, $uri = '')
	{
		// TODO: Implement payVehilceBill() method.
		return $this->sdkObj->payVehilceBill($params, $uri);
	}

	/**
	 * 手动开闸
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed
	 */
	public function openGate($params, $uri = '')
	{
		// TODO: Implement openGate() method.
		return $this->sdkObj->openGate($params, $uri);
	}

	/**
	 * 手动关闸
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed
	 */
	public function closeGate($params, $uri = '')
	{
		// TODO: Implement closeGate() method.
		return $this->sdkObj->closeGate($params, $uri);
	}

	/**
	 * 固定车位续费
	 * @param $params
	 * @param string $uri
	 * @return mixed
	 */
	public function carRecharge($params, $uri = '')
	{
		// TODO: Implement carRecharge() method.
		return $this->sdkObj->carRecharge($params, $uri);
	}

	/**
	 * 固定车位添加
	 * @param $params
	 * @param string $uri
	 * @return mixed
	 */
	public function addCarInfo($params, $uri = '')
	{
		// TODO: Implement addCarInfo() method.
		return $this->sdkObj->addCarInfo($params, $uri);
	}

	/**
	 * 固定车位取消
	 * @param $params
	 * @param string $uri
	 * @return mixed
	 */
	public function deleteCarInfo($params, $uri = '')
	{
		// TODO: Implement deleteCarInfo() method.
		return $this->sdkObj->deleteCarInfo($params, $uri);
	}

	/**
	 * 预约车位
	 * @param $params
	 * @param string $uri
	 * @return mixed ['parkName'=>'string 停车场名称','plateNo'=>'string 车牌号码', 'status'=>'int 0:已预约 1:用户取消 2:到期 3:已进场', 'startTime'=>'int 预约开始时间(时间戳)', 'endTime'=>'int 结束时间(时间戳)','phoneNo'=>'string 联系电话']
	 */
	public function reserveParkingPlot($params, $uri = '')
	{
		// TODO: Implement reserveParkingPlot() method.
		return $this->sdkObj->reserveParkingPlot($params, $uri);
	}

	/**
	 * 取消预约车位
	 * @param $params
	 * @param string $uri
	 * @return mixed
	 */
	public function cancelReserve($params, $uri = '')
	{
		// TODO: Implement cancelReserve() method.
		return $this->sdkObj->cancelReserve($params, $uri);
	}
}