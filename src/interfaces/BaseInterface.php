<?php
namespace Kpr\interfaces;

/**
 * 硬件车场商基础接口
 * Class BaseInterface
 * @package Kpr\interfaces
 */

abstract class BaseInterface{

	//* 此属性只能是config文件中的key
	public $nowFactory = '';

	//接口秘钥
	public $appKey = '';

	//接口密匙
	public $appScript = '';

	//是否加密
	public $isAddPwd = false;

	//接口请求地址
	public $apiUrl = '';

	//* 其他参数存放
	public $otherParams = [];

	//* 当前硬件API库版本号
	public $version='';

	//* 当前硬件库描述（某个厂家的某个模块服务，版本号，其他开发人员调用需要注意的点）
	public $describe='';

	//* 当前硬件库的所有请求路由列表
	public $allRouteList = [];

	/**
	 * 设置appKey
	 * @param $appKey
	 */
	public function setAppKey($appKey) {
		$this->appKey = $appKey;
	}

	/**
	 * 获取appKey
	 */
	public function getAppKey() {
		return $this->appKey;
	}

	/**
	 * 设置appScript
	 * @param $appScript
	 */
	public function setAppScript($appScript) {
		$this->appScript = $appScript;
	}

	/**
	 * 获取appScript
	 */
	public function getAppScript() {
		return $this->appScript;
	}

	/**
	 * 设置是否需要加密
	 * @param $isAddPwd
	 */
	public function setIsAddPwd($isAddPwd) {
		$this->isAddPwd = $isAddPwd;
	}

	/**
	 * 获取是否需要加密
	 */
	public function getIsAddPwd() {
		return $this->isAddPwd;
	}

	/**
	 * 设置apiUrl地址
	 * @param $apiUrl
	 */
	public function setApiUrl($apiUrl) {
		$this->apiUrl = $apiUrl;
	}

	/**
	 * 获取apiUrl地址
	 */
	public function getApiUrl() {
		return $this->apiUrl;
	}

	/**
	 * 设置其他参数
	 * @param $key
	 * @param $value
	 */
	public function addOtherParams($key, $value) {
		$this->otherParams[$key] = $value;
	}

	/**
	 * 设置其他参数
	 * @param string $key
	 * @param string $defaultValue
	 * @return array|mixed|string
	 */
	public function getOtherParams($key='', $defaultValue='') {
		if ($key == '') {
			return $this->otherParams;
		}

		return array_key_exists($key, $this->otherParams) ? $this->otherParams[$key] : $defaultValue;
	}

	/**
	 * 设置路由数组
	 * @param $key
	 * @param $value
	 */
	public function setAllRouteList($key, $value){
		if (array_key_exists($key, $this->allRouteList) && is_string($value)) {
			$this->allRouteList[$key] = $value;
		}
	}

	/**
	 * 获取路由
	 * @param $key
	 * @return mixed|string
	 */
	public function getAllRouteList($key) {
		return array_key_exists($key, $this->allRouteList) ? $this->allRouteList[$key] : '';
	}

	/**
	 * 移除其他参数
	 * @param $key
	 */
	public function deleteOtherParams($key) {
		if(array_key_exists($key, $this->otherParams)) {
			unset($this->otherParams[$key]);
		}
	}

	/**
	 * 获取当前硬件厂商文档版本号
	 * @return string
	 */
	public function getVersion() {
		return $this->version;
	}

	/**
	 * 获取当前硬件厂商的描述
	 * @return string
	 */
	public function getDescribe() {
		return $this->describe;
	}

	/**
	 * 构造函数 使用参数设置函数进行设置 注意此函数无返回值
	 * BaseInterface constructor.
	 * @param string $appKey
	 * @param string $appScript
	 * @param bool $isAddPwd
	 * @param string $apiUrl
	 */
	abstract public function __construct($appKey='', $appScript='', $isAddPwd=false, $apiUrl='');

	/**
	 * 查询停车场车位信息 剩余车位、总车位
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed [['total'=>'int 总车位','surplus'=>'int 剩余车位','regionName'=>'string 楼层名、区域名、地址名等','regionId'=>'string 如果存在楼层ID，区域ID，地址ID等填写，不存在则默认值-1'],]
	 */
	abstract public function getParkingInfos($params, $uri='');

	/**
	 * 查询车辆信息 车辆进场时间、停车时长
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed ['parkName'=>'string 停车场名称','plateNo'=>'string 车牌号','enterTime'=>'int 入场时间(时间戳)','parkingTime'=>'int 停车时长(秒)']
	 */
	abstract public function getCarTimeInfo($params, $uri='');

	/**
	 * 查询车辆停车费用
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed ['plateNo'=>'string 车牌号','parkName'=>'string 停车场名称', 'enterTime'=>'int 入场时间（时间戳）','parkingTime'=>'int 停车时长（秒）','cost'=>'float 应付金额','totalCost'=>'float 总收费金额', 'paidCost'=>'float 已支付金额','delayTime'=>'int 缴费后允许延时出场时间(秒)']
	 */
	abstract public function getChargeBill($params, $uri='');

	/**
	 * 费用上报 缴费成功后同步到硬件商服务器
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed ['totalCost'=>'float 总收费金额','realCost'=>'float 应收金额', 'cost'=>'float 实收金额','state'=>'int 支付状态 1:已支付 0:未支付', 'costTime'=>'int 缴费时间（时间戳）', 'parkName'=>'string 停车场名称']
	 */
	abstract public function payVehilceBill($params, $uri='');

	/**
	 * 手动开闸
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed
	 */
	abstract public function openGate($params, $uri='');

	/**
	 * 手动关闸
	 * @param $params
	 * @param $uri 当前方法对外访问的URI
	 * @return mixed
	 */
	abstract public function closeGate($params, $uri='');

	/**
	 * 固定车位续费
	 * @param $params
	 * @param string $uri
	 * @return mixed
	 */
	abstract public function carRecharge($params, $uri='');

	/**
	 * 固定车位添加
	 * @param $params
	 * @param string $uri
	 * @return mixed
	 */
	abstract public function addCarInfo($params, $uri='');

	/**
	 * 固定车位取消
	 * @param $params
	 * @param string $uri
	 * @return mixed
	 */
	abstract public function deleteCarInfo($params, $uri='');

	/**
	 * 预约车位
	 * @param $params
	 * @param string $uri
	 * @return mixed ['parkName'=>'string 停车场名称','plateNo'=>'string 车牌号码', 'status'=>'int 0:已预约 1:用户取消 2:到期 3:已进场', 'startTime'=>'int 预约开始时间(时间戳)', 'endTime'=>'int 结束时间(时间戳)','phoneNo'=>'string 联系电话']
	 */
	abstract public function reserveParkingPlot($params, $uri='');

	/**
	 * 取消预约车位
	 * @param $params
	 * @param string $uri
	 * @return mixed
	 */
	abstract public function cancelReserve($params, $uri='');
}